require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let!(:tshirt) { create(:base_product, name: "T SHIRT", price: 10.00) }
  let!(:yshirt) { create(:base_product, name: "Y SHIRT", price: 20.00) }
  let!(:tote)   { create(:base_product, name: "TOTE", price: 30.00) }

  feature 'a product page' do
    let(:shirt) { create(:taxon) }
    let(:bag)   { create(:taxon) }

    background do
      tshirt.taxons << shirt
      yshirt.taxons << shirt
      tote.taxons   << bag
    end

    scenario "a product page displays a specified product" do
      visit potepan_product_path(tshirt.id)

      expect(page).to     have_selector '.media-body', text: tshirt.name
      expect(page).to     have_selector '.media-body', text: tshirt.display_price
      expect(page).to     have_selector '.page-title', text: tshirt.name
      expect(page).to_not have_selector '.media-body', text: yshirt.name
      expect(page).to_not have_selector '.media-body', text: yshirt.display_price
      expect(page).to_not have_selector '.page-title', text: yshirt.name
      expect(page).to_not have_selector '.media-body', text: tote.name
      expect(page).to_not have_selector '.media-body', text: tote.display_price
      expect(page).to_not have_selector '.page-title', text: tote.name
    end

    scenario "a product page displays related products that have links to a specified product page" do
      visit potepan_product_path(tshirt.id)

      expect(page).to_not have_selector '.productBox', text: tshirt.name
      expect(page).to_not have_selector '.productBox', text: tshirt.display_price
      expect(page).to     have_selector '.productBox', text: yshirt.name
      expect(page).to     have_selector '.productBox', text: yshirt.display_price
      expect(page).to_not have_selector '.productBox', text: tote.name
      expect(page).to_not have_selector '.productBox', text: tote.display_price

      click_link yshirt.name

      expect(page).to     have_selector '.media-body', text: yshirt.name
      expect(page).to     have_selector '.page-title', text: yshirt.name
      expect(page).to     have_selector '.productBox', text: tshirt.name
      expect(page).to     have_selector '.productBox', text: tshirt.display_price
      expect(page).to_not have_selector '.productBox', text: yshirt.name
      expect(page).to_not have_selector '.productBox', text: yshirt.display_price
      expect(page).to_not have_selector '.productBox', text: tote.name
      expect(page).to_not have_selector '.productBox', text: tote.display_price
    end
  end
end
