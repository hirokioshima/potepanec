require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxon)             { create(:taxon) }
  let(:child_taxon)       { create(:taxon, name: "child taxon", parent: taxon) }
  let(:shirt_taxon)       { create(:taxon) }
  let(:bag)               { create(:base_product, price: 10.00) }
  let(:tote)              { create(:base_product, price: 20.00) }
  let(:tshirt)            { create(:base_product, price: 30.00) }
  let(:yshirt)            { create(:base_product, price: 40.00) }
  let(:type_tshirt_size)  { create(:option_type,  name: "tshirt-size",  presentation: "Size") }
  let(:type_tshirt_color) { create(:option_type,  name: "tshirt-color", presentation: "Color") }
  let(:type_yshirt_size)  { create(:option_type,  name: "yshirt-size",  presentation: "Size") }
  let(:type_yshirt_color) { create(:option_type,  name: "yshirt-color", presentation: "Color") }
  let(:value_small)       { create(:option_value, name: "Small",        presentation: "S") }
  let(:value_large)       { create(:option_value, name: "Large",        presentation: "L") }
  let(:value_blue)        { create(:option_value, name: "Blue",         presentation: "Blue") }
  let(:value_red)         { create(:option_value, name: "Red",          presentation: "Red") }
  let(:tshirt_variant)    { create(:base_variant, price: 30.00) }
  let(:yshirt_variant)    { create(:base_variant, price: 40.00) }

  background do
    bag.taxons  << taxon
    tote.taxons << child_taxon
    tshirt.taxons << shirt_taxon
    yshirt.taxons << shirt_taxon
    type_tshirt_size.option_values  << value_small
    type_tshirt_color.option_values << value_blue
    type_yshirt_size.option_values  << value_large
    type_yshirt_color.option_values << value_red
    tshirt_variant.option_values    << value_small
    tshirt_variant.option_values    << value_blue
    yshirt_variant.option_values    << value_large
    yshirt_variant.option_values    << value_red
    tshirt.variants << tshirt_variant
    yshirt.variants << yshirt_variant
  end

  scenario "a category page displays specified products" do
    visit potepan_category_path(taxon.id)

    expect(page).to have_current_path(potepan_category_path(taxon.id))
    expect(page).to have_selector '.page-title', text: taxon.name
    expect(page).to have_content   bag.name
    expect(page).to have_content   tote.name
    expect(page).to have_content   bag.display_price
    expect(page).to have_content   tote.display_price
    expect(page).to have_content   child_taxon.products.count
  end

  scenario "a category page has a correct link to a child taxon page that displays specified products" do
    visit potepan_category_path(taxon.id)

    click_link child_taxon.name

    expect(page).to     have_current_path(potepan_category_path(child_taxon.id))
    expect(page).to     have_selector '.page-title', text: child_taxon.name
    expect(page).to     have_content tote.name
    expect(page).to     have_content tote.display_price
    expect(page).to_not have_content bag.name
    expect(page).to_not have_content bag.display_price
  end

  scenario "a category page has a filter by color section that specifies products" do
    visit potepan_category_path(shirt_taxon.id)
    expect(page).to have_current_path(potepan_category_path(shirt_taxon.id))
    expect(page).to have_content tshirt.name
    expect(page).to have_content tshirt.display_price
    expect(page).to have_content yshirt.name
    expect(page).to have_content yshirt.display_price
    expect(page).to have_selector 'li a', text: value_blue.name

    click_link value_blue.name

    expect(page).to have_current_path(potepan_category_path(shirt_taxon.id,
                                        option_type: type_tshirt_color.presentation, option_value: value_blue.name))
    expect(page).to     have_content tshirt.name
    expect(page).to     have_content tshirt.display_price
    expect(page).to_not have_content yshirt.name
    expect(page).to_not have_content yshirt.display_price
  end

  scenario "a category page has a filter by size section that specifies products" do
    visit potepan_category_path(shirt_taxon.id)
    expect(page).to have_current_path(potepan_category_path(shirt_taxon.id))
    expect(page).to have_content tshirt.name
    expect(page).to have_content tshirt.display_price
    expect(page).to have_content yshirt.name
    expect(page).to have_content yshirt.display_price
    expect(page).to have_selector 'li a', text: value_small.name

    click_link value_small.name

    expect(page).to have_current_path(potepan_category_path(shirt_taxon.id,
                                        option_type: type_tshirt_size.presentation, option_value: value_small.name))
    expect(page).to     have_content tshirt.name
    expect(page).to     have_content tshirt.display_price
    expect(page).to_not have_content yshirt.name
    expect(page).to_not have_content yshirt.display_price
  end
end
