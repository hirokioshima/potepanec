require 'rails_helper'

feature "Limit products to display depending on when it was available" do
  background do
    create(:base_product, name: "1 MONTH AGO",  price: 100.00, available_on: 1.month.ago)
    create(:base_product, name: "4 MONTHS AGO", price: 200.00, available_on: 4.months.ago)
  end

  scenario "a home page doesn't display product older than 3 months" do
    visit potepan_path

    expect(page).to     have_content "人気カテゴリー"
    expect(page).to     have_content "1 MONTH AGO"
    expect(page).to     have_content 100.00
    expect(page).to_not have_content "4 MONTHS AGO"
    expect(page).to_not have_content 200.00
  end
end

feature "Limit products to display depending on the number of products" do
  background do
    create_list(:base_product, 7, available_on: 1.month.ago)
    create(:base_product, name: "DISPLAY_PRODUCT",    price: 100.00, available_on: Time.zone.now)
    create(:base_product, name: "NO_DISPLAY_PRODUCT", price: 200.00, available_on: 2.months.ago)
  end

  scenario "a home page displays products at most 8" do
    visit potepan_path

    expect(page).to     have_content "人気カテゴリー"
    expect(page).to     have_content "DISPLAY_PRODUCT"
    expect(page).to     have_content 100.00
    expect(page).to_not have_content "NO_DISPLAY_PRODUCT"
    expect(page).to_not have_content 200.00
  end
end
