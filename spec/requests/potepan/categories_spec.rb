require 'rails_helper'

RSpec.describe "Categories page", type: :request do
  describe "GET potepan/categories" do
    let(:bag)          { create(:base_product, price: 10.00) }
    let(:tote)         { create(:base_product, price: 20.00) }
    let(:tshirt)       { create(:base_product, price: 30.00) }
    let(:products)     { [bag, tote] }
    let(:taxon)        { create(:taxon) }
    let(:child_taxon)  { create(:taxon, parent: taxon) }
    let(:tshirt_taxon) { create(:taxon) }

    before do
      bag.taxons    << taxon
      tote.taxons   << child_taxon
      tshirt.taxons << tshirt_taxon
    end

    it "responds successfully" do
      get potepan_category_path(taxon.id)
      expect(response).to be_success
    end

    it "renders the correct template" do
      get potepan_category_path(taxon.id)
      expect(response).to render_template :show
    end

    it "assigns @taxon" do
      get potepan_category_path(taxon.id)
      expect(assigns(:taxon)).to eq taxon
    end

    it "displays related products" do
      get potepan_category_path(taxon.id)
      expect(response.body).to include "#{bag.name}"
      expect(response.body).to include "#{bag.display_price}"
      expect(response.body).to include "#{tote.name}"
      expect(response.body).to include "#{tote.display_price}"
    end

    it "does not display non related products" do
      get potepan_category_path(taxon.id)
      expect(response.body).to_not include "#{tshirt.name}"
      expect(response.body).to_not include "#{tshirt.display_price}"
    end

    it "assigns @category" do
      get potepan_category_path(taxon.id)
      expect(assigns(:category)).to eq products
    end

    describe "GET potepan/categories with options" do
      let(:option_type_tshirt_size) { create(:option_type,  name: "tshirt-size") }
      let(:option_value_small)      { create(:option_value, name: "Small") }
      let(:tshirt_variant)          { create(:base_variant, price: 30.00) }
      let(:params)                  {{ option_type: "Size", option_value: "Small" }}
      let(:category)                { [tshirt] }

      before do
        option_type_tshirt_size.option_values << option_value_small
        tshirt_variant.option_values << option_value_small
        tshirt.variants              << tshirt_variant
      end

      it "responds successfully" do
        get potepan_category_path(tshirt_taxon.id, params)
        expect(response).to be_success
      end

      it "renders the correct template" do
        get potepan_category_path(tshirt_taxon.id, params)
        expect(response).to render_template :show
      end

      it "assigns @category" do
        get potepan_category_path(tshirt_taxon.id, params)
        expect(assigns(:category)).to eq category
      end

      it "displays filtered products" do
        get potepan_category_path(tshirt_taxon.id, params)
        expect(response).to be_success

        expect(response.body).to include "#{tshirt.name}"
        expect(response.body).to include "#{tshirt.display_price}"
      end
    end
  end
end
