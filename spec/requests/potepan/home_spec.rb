require 'rails_helper'

RSpec.describe "Home page", type: :request do
  describe "GET potepan_path" do
    it "responds successfully" do
      get potepan_path
      expect(response).to be_success
    end

    it "renders the correct template" do
      get potepan_path
      expect(response).to render_template :index
    end
  end
end
