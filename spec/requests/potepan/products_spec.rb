require 'rails_helper'

RSpec.describe "Products page", type: :request do
  let(:tshirt) { create(:base_product) }
  let(:yshirt) { create(:base_product) }

  describe "GET potepan/products/:id" do
    let(:tote)  { create(:base_product) }
    let(:shirt) { create(:taxon) }
    let(:bag)   { create(:taxon) }

    before do
      tshirt.taxons << shirt
      yshirt.taxons << shirt
      tote.taxons   << bag
    end

    it "responds successfully" do
      get potepan_product_path(tshirt.id)
      expect(response).to be_success
    end

    it "renders the correct template" do
      get potepan_product_path(tshirt.id)
      expect(response).to render_template :show
    end

    it "assigns @product" do
      get potepan_product_path(tshirt.id)
      expect(assigns(:product)).to eq tshirt
    end

    it "assigns @related_products" do
      get potepan_product_path(tshirt.id)
      expect(assigns(:related_products)).to     include yshirt
      expect(assigns(:related_products)).to_not include tshirt, bag
    end
  end
end
