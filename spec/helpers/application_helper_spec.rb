require 'rails_helper'

RSpec.describe ApplicationHelper do
  include ApplicationHelper

  describe "#full_title" do
    let(:page_title) { "TEST TITLE" }

    it { expect(full_title).to eq "BIGBAG Store" }
    it { expect(full_title(page_title)).to eq "TEST TITLE - BIGBAG Store" }
  end
end
