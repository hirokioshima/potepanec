require 'rails_helper'

RSpec.describe Spree::OptionType, type: :model do
  let(:size)  { create(:option_type, presentation: "Size")  }
  let(:color) { create(:option_type, presentation: "Color") }

  it "specifies option_types by their presentations" do
    option_types = Spree::OptionType.specified_by_presentation("Color")
    expect(option_types).to     include color
    expect(option_types).to_not include size
  end
end
