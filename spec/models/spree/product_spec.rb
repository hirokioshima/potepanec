require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:tshirt) { create(:base_product, available_on: 1.month.ago)  }
  let!(:yshirt) { create(:base_product, available_on: 2.months.ago )}
  let!(:parker) { create(:base_product, available_on: 4.months.ago) }
  let!(:tote)   { create(:base_product, available_on: 5.months.ago) }

  describe 'methods using taxon' do
    let(:shirt) { create(:taxon) }
    let(:bag)   { create(:taxon) }
    before do
      tshirt.taxons << shirt
      yshirt.taxons << shirt
      tote.taxons   << bag
    end

    context '#without_product' do
      it "excludes a specified product" do
        products_without_tote = Spree::Product.without_product(tote)
        expect(products_without_tote).to     include tshirt, yshirt
        expect(products_without_tote).to_not include tote
      end
    end

    context '#join_taxons_and_specified_by' do
      it "specifies products by taxon" do
        shirts = Spree::Product.join_taxons_and_specified_by(shirt)
        expect(shirts).to     include tshirt, yshirt
        expect(shirts).to_not include tote
      end
    end

    context '#related_products' do
      it "specifies products by related product" do
        related_products = Spree::Product.related_products(tshirt)
        expect(related_products).to     include yshirt
        expect(related_products).to_not include tshirt, tote
      end
    end

    context '#filter_by_taxon' do
      it "specifies products by taxon" do
        filtered_products = Spree::Product.filter_by_taxon(shirt)
        expect(filtered_products).to     include tshirt, yshirt
        expect(filtered_products).to_not include tote
      end
    end

    describe 'methods using options' do
      let(:type_tshirt_size) { create(:option_type,  name: "tshirt-size", presentation: "Size") }
      let(:value_small)      { create(:option_value, name: "Small",       presentation: "S") }
      let(:options)          { { option_type: "Size", option_value: "Small" } }
      let(:tshirt_variant)   { create(:base_variant) }
      before do
        type_tshirt_size.option_values << value_small
        tshirt_variant.option_values   << value_small
        tshirt.variants << tshirt_variant
      end

      context '#filter_by_options' do
        it "specifies products by options" do
          filtered_products = Spree::Product.filter_by_options(options)
          expect(filtered_products).to match_array ( [tshirt] )
        end
      end

      context '#count_products_by' do
        it "counts products by taxon and value" do
          products_count = Spree::Product.count_products_by(shirt.id, value_small)
          expect(products_count).to eq 1
        end
      end
    end
  end

  describe 'methods using available_on' do
    context '#order_by_latest' do
      it "sorts products from latest to oldest" do
        products_sorted_from_latest = Spree::Product.order_by_latest
        expect(products_sorted_from_latest).to match (
         [tshirt, yshirt, parker, tote]
        )
      end
    end

    context '#available_within_past' do
      it "specifies products according to how many months ago they are availabled" do
        products_within_past_3_months = Spree::Product.available_within_past(3.months)
        expect(products_within_past_3_months).to match_array ( [tshirt, yshirt] )
      end
    end

    context '#order_by_latest_within_past' do
      it "specifies products according to how many months ago they are availabled and sorts from latest to oldest" do
        latest_products = Spree::Product.order_by_latest_within_past(3.months)
        expect(latest_products).to match (
          [tshirt, yshirt]
        )
      end
    end
  end
end
