Spree::Product.class_eval do
  scope :without_product,              -> (product) {
    where.not(id: product.id)
  }
  scope :join_taxons_and_specified_by, -> (taxons)  {
    joins(:taxons)
    .where(spree_taxons: {id: taxons})
  }
  scope :related_products,             -> (product) {
    join_taxons_and_specified_by(product.taxons)
    .without_product(product)
    .distinct
    .includes(master: [:default_price, :images])
  }
  scope :order_by_latest,              -> {
    order(available_on: :desc)
  }
  scope :available_within_past,        -> (time) {
    where('available_on > ?', time.ago)
  }
  scope :order_by_latest_within_past,  -> (time) {
    available_within_past(time)
    .order_by_latest
  }
  scope :filter_by_options,            -> (options) {
    joins(variants: :option_values)
    .where(spree_option_values: { name: options[:option_value] }) if options[:option_value].present?
  }

  scope :filter_by_taxon,              -> (taxon) {
    includes(master: [:default_price, :images])
    .in_taxon(taxon)
  }

  scope :count_products_by,            -> (taxon_id, value) {
    includes(variants: :option_values)
    .in_taxon(Spree::Taxon.find(taxon_id))
    .where(spree_option_values: { name: value.name })
    .count
  }
end
