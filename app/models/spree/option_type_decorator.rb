Spree::OptionType.class_eval do
  scope :specified_by_presentation, -> (presentation) { where(presentation: presentation)  }
end
