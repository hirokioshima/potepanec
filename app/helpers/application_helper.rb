module ApplicationHelper
  def full_title(page_title = '')
    base_title = "BIGBAG Store"
    if page_title.empty?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def page_title(taxon, value)
    if value.nil?
      taxon.name
    else
      "#{taxon.name} / #{value}"
    end
  end
end
