class Potepan::ProductsController < ApplicationController
  THE_NUMBER_OF_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @images  = @product.images
    @product_properties = @product.product_properties.includes(:property)
    @related_products   = Spree::Product
                            .related_products(@product)
                            .limit(THE_NUMBER_OF_PRODUCTS)
  end
end
