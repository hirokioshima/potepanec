class Potepan::HomeController < ApplicationController
  THE_NUMBER_OF_PRODUCTS = 8
  HOW_MANY_MONTHS_AGO    = 3.months

  def index
    @latest_products = Spree::Product.includes(master: [:default_price, :images])
                        .order_by_latest_within_past(HOW_MANY_MONTHS_AGO)
                        .limit(THE_NUMBER_OF_PRODUCTS)
  end
end
