class Potepan::CategoriesController < ApplicationController
  before_action :set_root_taxons, :set_filter_color, :set_filter_size
  helper_method :the_number_of_products_by

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @value = filter_params[:option_value]
    @category = Spree::Product
                  .filter_by_taxon(@taxon)
                  .filter_by_options(filter_params)
  end

  private

    def filter_params
      { option_type: params[:option_type], option_value: params[:option_value] }
    end

    def the_number_of_products_by(value)
      Spree::Product
        .count_products_by(params[:id], value)
    end

    def set_root_taxons
      @root_taxons = Spree::Taxon.roots
    end

    def set_filter_color
      @option_type_colors = Spree::OptionType
                              .includes(:option_values)
                              .specified_by_presentation("color")
    end

    def set_filter_size
      @option_type_sizes = Spree::OptionType
                            .includes(:option_values)
                            .specified_by_presentation("size")
    end
end
